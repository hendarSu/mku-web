var MiCom = {};

$(document).ready( function()
{
  if( $('.js-comment-count.-loading').length ) {
    MiCom.commentsCount();
  }

  if( $('.js-ajax-morenews').length ) {
    MiCom.ajaxMoreNews();
  }

  MiCom.setupMainMenu();

});

$(window).load( function()
{
  MiCom.equalheight('.m-gallery-archive-item');
});

// functions definition
MiCom.setupMainMenu = function()
{
  if( ! $('.js-main-menu').length ) return;

  // close menu when click outside of menu
  $('body').on('click', function(e) {
    $('.js-main-menu .dl-menu').removeClass('-news');
    $('.js-main-menu').data('dlmenu').closeMenu();
  });

  $('.js-main-menu').dlmenu({
    animationClasses : { classin : 'dl-animate-in', classout : 'dl-animate-out' }
  });

  // add custom class for styling news sub-menu (3 column)
  $('.js-main-menu .dl-menu li').on('click', function(e) {
    if( $(this).hasClass('-news') && ! $(this).hasClass('dl-subview-open') ) {
      $('.js-main-menu .dl-menu').addClass('-news');
    } else {
      $('.js-main-menu .dl-menu').removeClass('-news');
    }
  });
}

MiCom.ajaxMoreNews = function()
{
  $('.js-ajax-morenews').on('click', function(e) {
    e.preventDefault();

    var btn = $(this);
    if(btn.hasClass('disabled')) return;
    btn.addClass('disabled');

    $.ajax({
      type: 'GET',
      dataType: 'json',
      url: ajax_url + 'morenews?offset=' + $('.m-latest .item').length,
      success: function(data, textStatus, XMLHttpRequest) {
        // use existing element as template
        var item_template = $('.m-latest .item:first');

        $.each(data, function(idx, item) {
          var new_item = item_template.clone();

          new_item.find('a').attr('href', item.url);
          new_item.find('a.category').attr('href', item.category_url);
          new_item.find('img').attr('src', item.thumbnail);
          new_item.find('.title').html(item.title);
          new_item.find('.category').html(item.category);
          new_item.find('.excerpt').html(item.summary);
          new_item.find('.views').html('<i class="fa fa-eye"></i> ' + item.hits);
          new_item.find('.time').html(item.time);
          new_item.find('.js-comment-count')
            .addClass('-loading')
            .data('disqus-url', 'http://' + http_host + item.url)
            .html('...');

          new_item.appendTo('.m-latest > .items')
          .hide()
          .slideDown('250');
        });
        btn.removeClass('disabled');

        MiCom.commentsCount();
      }
    });
  });
}

MiCom.commentsCount = function()
{
  var urlArray = [];
  $('.js-comment-count.-loading').each(function () {
    var url = $(this).data('disqus-url');
    urlArray.push('link:' + url);
  });

  jQuery.ajax({
    url: ajax_url + "commentscount",
    data: { thread : urlArray },
    dataType: 'json',
    success: function (result) {
      for (var i in result) {
        $('.js-comment-count[data-disqus-url="' + result[i].link + '"]')
          .removeClass('-loading')
          .html( result[i].posts );
      }

      $('.js-comment-count.-loading')
        .removeClass('-loading')
        .html('0');
    }
  });
}

// http://codepen.io/micahgodbolt/pen/FgqLc
MiCom.equalheight = function(container)
{
  var currentTallest = 0,
     currentRowStart = 0,
     rowDivs = new Array(),
     $el,
     topPosition = 0;
  $(container).each(function() {
    $el = $(this);
    $($el).height('auto')
    topPostion = $el.position().top;

    if (currentRowStart != topPostion) {
     for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
       rowDivs[currentDiv].height(currentTallest);
     }
     rowDivs.length = 0; // empty the array
     currentRowStart = topPostion;
     currentTallest = $el.height();
     rowDivs.push($el);
   } else {
     rowDivs.push($el);
     currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
  }
   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
     rowDivs[currentDiv].height(currentTallest);
   }
 });
}