(function($) {
  var fake = $.ajax.fake;
  
  fake.registerWebservice('http://www.example.com/more', function(data) {
    return [
        {
          'category': 'Ekonomi',
          'url': '#',
          'thumbnail': 'inc/placeholder/120-80-11',
          'title': 'Inflasi DKI Jakarta Sentuh 0,01%',
          'excerpt': 'Rejeki menjadi orang kaya sebetulnya bisa ditelaah secara ilmu pengetahuan, namun kebanyakan hanya teori',
          'time': '1 Oktober 2015, 12:15 WIB',
          'views': '10K',
          'comments': '500'
        },
        {
          'category': 'Finance',
          'url': '#',
          'thumbnail': 'inc/placeholder/120-80-12',
          'title': 'TAJUK BISNIS: Menjaga Daya Tahan UKM',
          'excerpt': 'JAKARTA - Pada bulan ini, pemerintah telah dua kali mengeluarkan paket kebijakan ekonomi yang tentunya diharapkan mampu',
          'time': '1 Oktober 2015, 12:15 WIB',
          'views': '10K',
          'comments': '500'
        },
        {
          'category': 'Politik',
          'url': '#',
          'thumbnail': 'inc/placeholder/120-80-13',
          'title': 'IHSG Menguat Dekati Level 4.2000',
          'excerpt': 'JAKARTA - Bursa saham tanah air, pagi ini dibuka mengalami kenaikan. Indeks Harga Saham Gabungan naik 9,72.',
          'time': '1 Oktober 2015, 12:15 WIB',
          'views': '10K',
          'comments': '500'
        },
        {
          'category': 'Ekonomi',
          'url': '#',
          'thumbnail': 'inc/placeholder/120-80-14',
          'title': 'BPS: September, Deflasi 0,05%, Harga Bahan Makanan Menuju Normal',
          'excerpt': 'JAKARTA - Kepala Badan Pusat Statistik (BPS) Suryamin mengatakan bahan makanan menjadi penyumbang deflasi September 2015 yang',
          'time': '1 Oktober 2015, 12:15 WIB',
          'views': '10K',
          'comments': '500'
        },
        {
          'category': 'Publik',
          'url': '#',
          'thumbnail': 'inc/placeholder/120-80-15',
          'title': 'Ini Bedanya Ladyjek dengan Go-Jek, GrabBike, Blujek',
          'excerpt': 'JAKARTA - Pada bulan ini, pemerintah telah dua kali mengeluarkan paket kebijakan ekonomi yang tentunya diharapkan mampu menjadi',
          'time': '1 Oktober 2015, 12:15 WIB',
          'views': '10K',
          'comments': '500'
        }
      ]
  });
})(jQuery);